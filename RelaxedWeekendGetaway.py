import math
from typing import List, Optional

def validate_inputs(preferences: List[List[int]], licences: List[int]) -> bool :
    """
    Function description: Check if the inputs (preferences and licences) are valid and non-empty and that no
    preference list is empty.

    Approach description: It checks for the non-emptiness of preferences and licences and confirms no inner list within
    preferences is empty by looping through them, providing a quick validity check of inputs.

    :Input:
    preferences: A list of lists containing integer preferences.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A boolean indicating whether the inputs are valid.

    :Time complexity: O(n), where n is the number of persons (size of preferences list).

    :Aux space complexity: O(1)
    """
    return bool(preferences) and bool(licences) and not any(len(p) == 0 for p in preferences)  # O(n) time complexity


def initialize_allocation(n: int, licences: List[int]) -> (List[List[int]], List[int]) :
    """
    Function description: Initialize the allocation, unallocated persons, and drivers list, using the input
    size n and a copy of the licences list.

    Approach description: Given the total number of persons n and a list licences, it determines the number of cars d
    and returns three lists: an empty allocation structure (a list of empty lists), a list of all persons as
    initially unallocated, and a copy of licences.

    :Input:
    n: The number of persons to be allocated.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A tuple containing an initial empty allocation list, a list of all
    unallocated persons, and a copy of the drivers' list.

    :Time complexity: O(n), where n is the number of persons to be allocated.

    :Aux space complexity: O(n), where n is the number of persons to be allocated.
    """
    d = math.ceil(n / 5)  # Number of destinations/cars
    return [[] for _ in range(d)], list(range(n)), licences.copy()  # O(n) time complexity


def allocate_drivers(allocation: List[List[int]], unallocated: List[int], drivers: List[int],
                     preferences: List[List[int]], d: int) -> bool :
    """
    Function description: Allocate at least two drivers to each car while considering their preferences.

    Approach description: It iterates through each vehicle and driver, trying to allocate at least two drivers per
    vehicle based on their preferences, updating allocation, unallocated, and drivers in the process. If it can't
    assign two drivers to a car, it returns False.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    unallocated: A list of persons yet to be allocated.
    drivers: A list of persons with a driving license.
    preferences: A list of lists containing the preferences of each person.
    d: The total number of destinations/cars.

    :Output, return or postcondition: A boolean indicating whether at least two drivers could be allocated
    to each car.

    :Time complexity: O(d * l * n), where d is the number of destinations/cars, l is the number of licences,
    and n is the number of persons. In the worst case, this part could iterate over all drivers for all cars.

    :Aux space complexity: O(1)
    """
    for car in range(d) :  # O(n) time complexity
        allocated_drivers = 0
        for i in drivers[:] :  # Using slicing to avoid modifying the iterating list directly: O(n) time complexity
            if allocated_drivers >= 2 :  # Once two drivers are allocated, move to the next car.
                break
            if car in preferences[i] and i in unallocated :  # O(n) time complexity
                allocation[car].append(i)
                unallocated.remove(i)  # O(n) time complexity
                drivers.remove(i)  # O(n) time complexity
                allocated_drivers += 1
        if allocated_drivers < 2 :  # Cannot ensure two drivers for a car
            return False
    return True


def allocate_remaining(allocation: List[List[int]], unallocated: List[int], preferences: List[List[int]]) -> bool :
    """
    Function description: Allocate the remaining persons according to their preferences, ensuring no car is
    over-occupied.

    Approach description: For each unallocated person, it iterates through their preferences, trying to allocate them
    to a car if it doesn’t exceed the maximum occupancy (5). If a person cannot be allocated, the function returns
    False.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    unallocated: A list of persons yet to be allocated. preferences: A list of lists containing the preferences of
    each person.

    :Output, return or postcondition: A boolean indicating whether all remaining persons could be allocated according to
     their preferences.

    :Time complexity: O(u * p), where u is the number of unallocated persons and p is the max length of a preference
    list.

    :Aux space complexity: O(1)
    """
    for i in unallocated[:] :  # To safely modify unallocated during iteration: O(n) time complexity
        allocated = False
        for dest in preferences[i] :  # O(n) time complexity
            if len(allocation[dest]) < 5 :  # Ensure the car is not fully booked.
                allocation[dest].append(i)
                unallocated.remove(i)  # O(n) time complexity
                allocated = True
                break
        if not allocated :  # If a person could not be allocated to any preferred destination.
            return False
    return True


def validate_final_allocation(allocation: List[List[int]], licences: List[int]) -> bool :
    """
    Function description: Validate the final allocations, ensuring each car has a valid number of occupants and at least
    one licensed driver.

    Approach description: It iterates through each car in allocation, checking if the number of persons is between 2 and
    5 and if there’s at least one licensed driver, ensuring the solution adheres to problem constraints.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A boolean indicating whether the final allocation is valid.

    :Time complexity: O(d * m), where d is the number of destinations/cars and m is the max number of members in a car.

    :Aux space complexity: O(1)
    """
    for car in allocation :  # O(n) time complexity
        if len(car) < 2 or len(car) > 5 :  # Validate car occupation
            return False
        if not any(i in licences for i in car) :  # Ensure at least one licensed driver
            return False
    return True


def allocate(preferences: List[List[int]], licences: List[int]) -> Optional[List[List[int]]] :
    """
    Function description: The 'allocate' function assigns individuals to vehicles, adhering to specific rules and
    preferences, and ensuring that each vehicle has a minimum of two drivers. It navigates through several stages:
    initializing allocation based on provided preferences and licenses, allocating at least two drivers per vehicle,
    assigning the remaining individuals according to their preferences, and validating the final allocation to make
    sure all the requirements (like car occupancy and licensed driver presence) are met.

    Approach description:  The function employs a greedy approach. Initially, the function validates inputs and sets up
    basic allocations while ensuring every vehicle has at least two drivers, prioritizing their preferences. After
    driver allocation, it attempts to assign the remaining individuals according to their respective preferences while
    maintaining a cap on vehicle occupancy. Post allocation, a final validation checks if each vehicle has between 2 and
    5 individuals and at least one licensed driver, ensuring the devised solution adheres to the stipulated constraints.
    If any step fails, the function promptly returns None; otherwise, it outputs the achieved allocation.

    :Input:
    preferences: A list of lists containing the preferences of each person.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A list of lists representing the final allocation if possible, otherwise None.

    :Time complexity: The total worst case time complexity equals to O(n + d x l x n + u x p + d x m)
    We know that:
    d is the number of destinations/cars, since there are ⌈n/5⌉ cars and destinations, and thus d = ⌈n/5⌉ at maximum,
    simplifying to O(n) in the worst case.
    l is the length of 'licences' and in the worst case, every person has a license so l = n.
    u is the number of unallocated persons, since the worst-case scenario here is that no one is allocated initially, so
    u is bounded by n.
    p is the max length of a preference list, which is bounded by d, as persons will indicate their preferred
    destinations, which are numbered 0 through ⌈n/5⌉ - 1. So, everyone could have a preference for all available
    destinations. Thus, p is bounded by d, which is ⌈n/5⌉.
    m is the max number of members in a car, which is a constant number (5) and does not grow with n, so we can
    disregard it in big O notation.
    Substituting these back in: O(n + (n x n x n) + (n x n) + n) = O(n^3 + n^2+ 2n) = O(n^3), where n is the number of
    persons.

    :Aux space complexity: O(n), where n is the number of persons.
    """
    n = len(preferences)

    if not validate_inputs(preferences, licences) :
        return None

    allocation, unallocated, drivers = initialize_allocation(n, licences)
    d = math.ceil(n / 5)

    if not allocate_drivers(allocation, unallocated, drivers, preferences, d) :
        return None

    if not allocate_remaining(allocation, unallocated, preferences) :
        return None

    if not validate_final_allocation(allocation, licences) :
        return None

    return allocation

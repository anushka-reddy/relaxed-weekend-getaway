# Weekend Getaway Solution README

## Overview
The Weekend Getaway solution program helps organize a weekend trip for a group of friends while considering constraints such as minimizing the number of cars used, ensuring drivers are available, and accommodating everyone's destination preferences. The program takes as input the preferences of each person and their driver license status, then allocates individuals to cars and destinations based on these constraints.

## Usage Instructions
1. **Input Format:**
   - The program expects two lists as input:
     - `preferences`: A list of lists where each inner list represents the destination preferences of a person. The elements of each inner list indicate the destinations (indexed from 0 to ⌈n/5⌉ - 1) the person is interested in.
     - `licences`: A list of indices representing the individuals who have driver licenses.

2. **Output Format:**
   - If it is possible to allocate persons into cars/destinations while satisfying all constraints, the program returns a list of lists `cars` where each inner list identifies the persons traveling in a specific car to a particular destination.
   - If it is impossible to allocate persons into cars/destinations while satisfying all constraints (e.g., if any person has an empty preference list), the program returns `None`.

3. **Example:**
   - **Input:**
     ```python
     preferences = [[0], [1], [0, 1], [0, 1], [1, 0], [1], [1, 0], [0, 1], [1]]
     licences = [1, 4, 0, 5, 8]
     ```
   - **Output:**
     ```python
     [[0, 4, 3, 2], [1, 5, 6, 7, 8]]
     ```

4. **Complexity:**
   - The solution has a worst-case time complexity of O(n^3).

For further details or assistance, please refer to the documentation or contact the developer.

*Developer: Anushka Reddy*
*Contact: anushkar614@gmail.com*
